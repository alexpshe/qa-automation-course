package com.automation.qa.lesson1;

public class SecondTask {
    public static void main(String[] args) {
        createVariables();
    }

    /**
     * Задание №2
     * Напишите программу, которая в методе main объявляет такие переменные: count типа byte, age типа int и starts типа long.
     *
     * Примечание: "объявить переменную" - значит то же, что и "создать переменную".
     */
    public static void createVariables() {
        // писать решение здесь
    }
}
